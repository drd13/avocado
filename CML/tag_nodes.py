#!/usr/bin/env python
"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""

class Node:
    def __init__(self, tag=None, start=None, end=None, nodes=None, next=None, previous=None):
        self.tag = tag
        self.next = next
        self.previous = previous
        self.start = start
        self.end = end
        self.nodes = nodes
        self.is_head = True

    def __str__(self):
        return str(self.tag)

    def return_tag(self):
        return self.tag

    def has_head(self):

        #check if there is a next node
        if self.next == False:
            return False

        start_node, current_node = self, self.next

        #iterate through nodes in the list and check if any of them have been assigned as a head
        while current_node.tag != start_node.tag:          

            if current_node.is_head == True:
                return True

            current_node = current_node.next

        return False

    def return_tail(self):

        current_node = self
        while current_node.next:
            current_node = current_node.next
        return current_node

    def print_nodes(self):

        print self.tag

        if self.next != None:
            head = self.next
            head.print_nodes()

    def return_list(self, container):

        container.append(self.tag)

        if self.next != None:
            head = self.next
            head.return_list(container)

class LinkedList:
    def __init__(self):
        self.length = 0
        self.head = None
        self.tail = None

    def add_node_start(self, val):
        node = Node(val)
        node.next = self.head
        if self.head:
            self.head.previous = node

        self.head = node

        if (self.length == 0):
            self.tail = node
        
        self.length += 1

    def add_node_end(self, val):
        node = Node(val) 
        if self.tail:        
            node.previous = self.tail
            self.tail.next = node
        self.tail = node
        
        if (self.length == 0):
            self.head = node

        self.length += 1

    def return_head_tail(self):
        return [self.head, self.tail]

    def return_list(self):
    
        store = []

        if self.head != None:
            self.head.return_list(store)

        return store
