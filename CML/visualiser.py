#!/usr/bin/env python

"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""

try:
    from parser_fluidity import parse
except ImportError:
    print "Failed to import parser fluidity."
try:
    from tag_nodes import Node
    from tag_nodes import LinkedList
except ImportError:
    print "Failed to import tag nodes class."
from string import Template
try:
    import simplekml
except ImportError:
    print "Failed to import simplekml."
try:
    import matplotlib.pyplot as plt
    import matplotlib
except ImportError:
    print "Failed to import matplotlib."
import itertools
try:
    import numpy
except ImportError:
    print "Failed to import numpy."
import scipy.interpolate
try:
    import GFD_basisChangeTools as GFD
except ImportError:
    print "Failed to import GFD library."
from mpl_toolkits.mplot3d import Axes3D
import math
from collections import defaultdict
from scipy.io import netcdf
try:
    import Tkinter
except ImportError:
    print "Failed to import TKinter library!"
try:
    from PIL import Image
except ImportError:
    print "Failed to import PIL library."
import os
try:
    from image_handler import imcrop
except ImportError:
    print "Failed to import imcrop from image_handler - make sure file is present!"
try:
    import matplotlib as mpl
    from matplotlib.collections import PolyCollection
except ImportError:
    print "Failed to import matplotlib."
try:
        import vtktools
except ImportError:
        print 'WARNING: Can not import vtktools, some features will not work'
try:
    import ogr, osr
except ImportError:
    print "Failed to import ogr and osr libraries."

class simulation:
    def __init__(self, file_dictionary):

        self.file_dictionary = file_dictionary
        self.run_path = self.return_directory()
        self.parser = parse(self.file_dictionary)
        self.mesh, self.data = self.populate_all_data()
        self.kml = self.create_joined_kml()

    def print_directory(self):
        print self.run_path

    def return_directory(self):
        return os.path.abspath(os.path.join(__file__,"../.."))

    def populate_all_data(self):

        self.parser.parse_all()

        print "Populated mesh and data!"

        return self.parser.return_mesh_data(), self.parser.return_data()

    def show_outer_boundary(self):

        outer = self.mesh['Outer boundary']

        print len(outer)

        plt.clf()

        x, y = [], []
        for xy in outer:
            x.append(xy[0])
            y.append(xy[1])

        plt.scatter(x,y)
        plt.show()

    def export_mesh(self, doc):

        multi_geo, triangle_mesh = doc.newmultigeometry(name = 'Mesh'), self.mesh['Mesh']
    
        fig, ax = plt.subplots()

        coll = PolyCollection(triangle_mesh, cmap=mpl.cm.jet, edgecolors='black', facecolors = 'white')
        ax.add_collection(coll)
        ax.autoscale_view()
        self.save_image(os.path.join(self.run_path,'Images','mesh.png'), fig, 600)

    def export_nodes(self):
        outfile = open('all_nodes.txt', 'w')

        for i in self.nodes:
            outfile.write(str(i[0])+','+str(i[1])+'\n')

        outfile.close()

    def create_joined_kml(self):
        
        print 'Generating KML file!'

        kml = simplekml.Kml()
        doc = kml.newdocument(name='Med')
        
        self.create_images()
    
        print '-created images.'

        self.create_overlay_kml(doc)

        print '-created overlay.'

        fol = doc.newfolder(name = 'Boundary')

        self.create_boundary_linestring(fol)
        print '--> generated linestring.'
        kml.save(os.path.join(self.run_path,'KML','all_med.kml'))
        print '-Saved file.'
        if os.path.isfile(os.path.join(self.run_path,'KML','all_med.kml')):
            return os.path.join(self.run_path,'KML','all_med.kml')
        else: return None
        

    def create_overlay_kml(self, doc):        
    
        if ('Boundary conditions' in self.mesh):

            bounding_mesh = self.mesh['Boundary conditions']

            if (bounding_mesh):

                x_1 = bounding_mesh[0]
                y_1 = bounding_mesh[2]

                x_2 = bounding_mesh[1]
                y_2 = bounding_mesh[3]

                p1, p2, p3, p4 = [x_1, y_1], [x_1, y_2], [x_2, y_2], [x_2, y_1]

                fol = doc.newfolder(name = 'Results')

                #for each type of data we have overlay the images we have generated as well as the colorbar
                for data_type in self.data:

                    new_fol = fol.newfolder(name=data_type)

                    terrain = new_fol.newgroundoverlay(name=data_type, color = '7fffffff', visibility = 0)
                    terrain.icon.href = os.path.join(self.run_path,'Images',data_type+'.png')
                    terrain.gxlatlonquad.coords = [p1, p4, p3, p2]

                    screen = new_fol.newscreenoverlay(name=data_type+' color bar', color = '7fffffff', visibility = 0)
                    screen.icon.href = os.path.join(self.run_path,'Images',data_type+'_colorbar.png')

                    screen.screenxy = simplekml.ScreenXY(x=0.05,y=0.99,xunits=simplekml.Units.fraction,yunits=simplekml.Units.fraction)
                    screen.overlayxy = simplekml.OverlayXY(x=0.05,y=0.99,xunits=simplekml.Units.fraction,yunits=simplekml.Units.fraction)

                    w, h = self.return_image_width(os.path.join(self.run_path,'Images',data_type+'_colorbar.png'), 450), 450

                    screen.size.x = w
                    screen.size.y = h
                    screen.size.xunits = simplekml.Units.pixel
                    screen.size.yunits = simplekml.Units.pixel

            else:
                print "Failed to load boundary conditions!"
        else:
            print "No boundary conditions loaded!"

    def create_boundary_points(self, fol):

        if ('Nodes' in self.mesh):

            geo = fol.newmultigeometry(name = 'Node area')

            nodes = self.mesh['Nodes']

            for x, y in zip(nodes[0], nodes[1]):
                pnt = geo.newpoint()
                pnt.coords = [(x,y)]
        else:
            print "Nodes was not found in the tag dictionary, failed to generate node boundary!"

    def create_images(self):

        #iterate through all the data types the parser has provided
        for data_type in self.data:

            print data_type
            
            #get the x/y/z of the data type
            x = self.data[data_type][0]
            y = self.data[data_type][1]
            z = self.data[data_type][2]

            nx = 200
            ny = 200
        
            #interpolate the values between the ones provided using RBF interpolation
            xi, yi = numpy.linspace(min(x), max(x), nx), numpy.linspace(min(y), max(y), ny)
            xi, yi = numpy.meshgrid(xi, yi)
        
            rbf = scipy.interpolate.Rbf(x, y, z, function = 'linear')
            zi = rbf(xi, yi)

            fig2 = plt.figure()
            ax2 = fig2.add_subplot(111)

            cax = ax2.imshow(zi, vmin = min(z), vmax = max(z), origin = 'lower', extent = [min(x), max(x), min(y), max(y)])

            #generate a color bar for the image
            c_fig = plt.figure()
            cbar = c_fig.colorbar(cax)
            plt.gca().set_visible(False)

            #save both the images using the name and colorbar name if necessary
            self.save_image(os.path.join(self.run_path,'Images',data_type+'_colorbar.png'), c_fig, 600)
            self.save_image(os.path.join(self.run_path,'Images',data_type+'.png'), fig2, 600)

            #convert the boundary to an xy polygon for the image (convert long/lat to the pixel x and y equivalent
            boundary = self.convert_to_polygon(os.path.join(self.run_path,'Images',data_type+'.png'))

            #if the boundary exists, crop the image to the polygon we have generated
            if boundary:
                img = imcrop(os.path.join(self.run_path,'Images',data_type+'.png'), boundary)
                img.crop_polygon(os.path.join(self.run_path,'Images',data_type+'.png'))
            else:
                print "Failed to crop the images down to boundary nodes!"
            
    def return_image_width(self, path, height):
        img = Image.open(path)

        width, height_img = img.size
        ratio = float(width)/height_img

        new_width = int(height*ratio)

        return new_width

    def save_image(self, name, fig, dpi):
        fig_size = fig.get_size_inches()
        w, h = fig_size[0], fig_size[1]
        fig.patch.set_alpha(0)
        a=fig.gca()
        a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('off')
        fig.savefig(name, transparent=True, bbox_inches='tight', pad_inches=0, dpi = dpi)

    def create_placemark(self, coords, place_name, description, *name):

        if name[0]:
            file_name = str(name[0])+'.kml'
        else:
            file_name = str(self.filename)+'.kml'
                
        
        file_temp = open('placemark.kml')
        org_src = Template(file_temp.read())

        rep = {'name':place_name, 'description':description, 'coords':','.join(coords)}

        replaced = org_src.substitute(rep)

        out_file = open(file_name, 'w')
        out_file.write(replaced)

    def convert_coords_to_image_coords(self, resolution, xy):
        
        try:

            bounding_mesh = self.mesh['Boundary conditions']

            #check if the tag exists and return the values of the bounding rectangle for the region
            if (bounding_mesh):

                x_1 = bounding_mesh[0]
                y_1 = bounding_mesh[2]

                x_2 = bounding_mesh[1]
                y_2 = bounding_mesh[3]

                p1, p2, p3, p4 = [x_1, y_1], [x_1, y_2], [x_2, y_2], [x_2, y_1]       

                d_y = abs(p2[1]-p1[1]) 
                d_x = abs(p3[0]-p2[0])

            width = resolution[0]
            height = resolution[1]

            new_x = (((xy[0]-p1[0])/d_x) * width)
            new_y = height - (((xy[1]-p1[1])/d_y) * height)
    
            return (new_x, new_y)

        except KeyError:
            print "Boundary conditions not found, failed to generate image!"
        

    def convert_to_polygon(self, image):
        
        try:

            node_list = self.mesh['Outer boundary']
            poly = []

            im = Image.open(image)
            if im:
                width, height = im.size
        
            #go through nodes and convert from long/lat to pixel x/y of the image
            for nodes in node_list:

                current = []

                for node in nodes:
            
                    new = self.convert_coords_to_image_coords([width,height], node)
                    current.append(new)

                poly.append(current)

            return poly

        except KeyError:
            print "Failed to load boundary tag, can not generate polygon!"

    def plot_all_tags(self, fol):
        
        if ('Tag dictionary' in self.mesh):

            geo = fol.newmultigeometry(name = 'Tag dictionary')

            bound = self.mesh['Tag dictionary']

            for tag in bound:
                for node_pair in bound[tag]:
                    ls = geo.newlinestring(coords = [node_pair[0],node_pair[1]])
                    ls.style.linestyle.width = 3

    def plot_outer_boundary(self, fol):
            
        if ('Outer boundary' in self.mesh):

            geo = fol.newmultigeometry(name = 'Outer boundary')

            bound = self.mesh['Outer boundary'][0]

            #create first last connection
            ls = geo.newlinestring(coords = [bound[0], bound[-1]])
            ls.style.linestyle.width = 3
        
            previous_node = None

            for current_node in bound:
                if previous_node:                    
                    ls = geo.newlinestring(coords = [previous_node, current_node])
                    ls.style.linestyle.width = 1.5

                previous_node = current_node
            

    def create_boundary_linestring(self, fol):
        
        if ('Outer boundary' in self.mesh):

            geo = fol.newmultigeometry(name = 'Line boundary')

            boundary_dictionary = self.mesh['Outer boundary']

            geo.style.linestyle.width = 1.5

            #generates just the outer boundary
            if (len(boundary_dictionary) > 0):

                for nodes in boundary_dictionary:

                    closed_nodes = nodes
                    closed_nodes.append(nodes[0])
                    ls = geo.newlinestring(coords = closed_nodes)

            else:
                print "Failed to load enough tags!"
        else:
            print "Tag dictionary is not loaded! Most likely missing .msh file!"


if __name__ == '__main__':

    data = {'.pvtu' : ['/home/jl11313/ese-home/RDM_files/simulation/Velocity2d_28.pvtu'], '.msh' : ['/home/jl11313/ese-home/RDM_files/simulation/med.msh'], 'nc' : ['/home/jl11313/ese-home/RDM_files/simulation/med.nc'], '.grd' : ['/home/jl11313/ese-home/RDM_files/simulation/globesampled.grd']}

    data2 = {'.msh' : ['/home/jl11313/ese-home/for_Jan/mesh/westernSvalbardMesh.msh'], '.nc' : ['/home/jl11313/ese-home/for_Jan/fluidityBathymetry.nc'], '.pvtu' : ['/home/jl11313/ese-home/for_Jan/westSv1000_720.pvtu'], '.flml' : ['/home/jl11313/ese-home/for_Jan/landslide.flml']}

    sim = simulation(data)
