#!/usr/bin/env python
"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
import os
import numpy
try:
    from PIL import Image, ImageDraw
except ImportError:
    print "Failed to import Image and ImageDraw from PIL. PIL library not found."
    
try:
    import matplotlib.pyplot as plt
except ImportError:
    print "Failed to import matplotlib."

class imcrop:

    def __init__(self, path, boundary):
        self.path = path
        self.boundary = boundary
        self.im = self.return_image()
        self.mask = self.generate_mask()

    def generate_mask(self):
        pass

    def return_image(self):
        #convert the image to a numpy array and add 4th column over RGB (transparency).
        return numpy.asarray(Image.open(self.path).convert('RGBA')) 

    def crop_polygon(self, filepath):

        #create a new mask array which is the same size as our image array and draw the polygon over it, adding a fill
        maskIm = Image.new('L', (self.im.shape[1], self.im.shape[0]),0)
        ImageDraw.Draw(maskIm).polygon(self.boundary[0], outline = 1, fill = 1)
        #iterate through the boundary polygons (mesh boundary polygons) and draw them on - these are the inner boundaries
        for bounds in self.boundary[1:]:
            ImageDraw.Draw(maskIm).polygon(bounds, outline = 0, fill = 0)

        #copy the mask
        mask = numpy.array(maskIm)
        new_image_array = numpy.empty(self.im.shape,dtype='uint8')
        #copy the RGB values into the new image, but add a transparency value to the polygons added
        new_image_array[:,:,:3] = self.im[:,:,:3]
        new_image_array[:,:,3] = mask*255

        newIm = Image.fromarray(new_image_array, 'RGBA')
        newIm.save(filepath)
