#!/usr/bin/env python

"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.

"""

from string import Template
try:
    import simplekml
except ImportError:
    print "Failed to load simplekml library."
try:
    import matplotlib.pyplot as plt
    import matplotlib
except ImportError:
    print "Failed to import matplotlib."
import itertools
try:
    import numpy as np
except ImportError:
    print "Failed to import numpy."
import scipy.interpolate
try:
    import GFD_basisChangeTools as GFD
except ImportError:
    print "Failed to import GFD library."
import math
from collections import defaultdict
from scipy.io import netcdf
try:
    from PIL import Image
except ImportError:
    print "Failed to import PIL library."
import os
try:
    from tag_nodes import Node
    from tag_nodes import LinkedList
except ImportError:
    print "Failed to import tag nodes class."
from scipy.io import netcdf
try:
    import xml.etree.ElementTree as etree
except ImportError:
    print "Failed to import element tree."
import sys
try:
    from XML_handler import XML
except ImportError:
    print "Failed to import XML handler."

try:
        import vtktools
except:
        print 'WARNING: vtktools can not be imported, some features will not work'
import ogr, osr

class parse:

    def __init__(self, file_dictionary):

        self.file_dictionary = file_dictionary
        self.flml = None
        self.mesh = {}
        self.data = {}

    def return_paths(self):
        return self.file_dictionary

    def return_data_types(self):
        for key in self.data:
            print key

    def transform_points(self, data, src, tgt):

        #have an iterator as well as the x,y of the data
        for i, (x, y) in enumerate(zip(data[0], data[1])):

                source = osr.SpatialReference()
                source.ImportFromEPSG(src)
                target = osr.SpatialReference()
                target.ImportFromEPSG(tgt)
                #apply transformation from osurce to target on the point
                transform = osr.CoordinateTransformation(source, target)
                point = ogr.Geometry(ogr.wkbPoint)
                point.AddPoint(x, y)
                
                point.Transform(transform)
                #change the value of data with iterator and return it
                data[0][i], data[1][i] = point.GetX(), point.GetY()

        return data

    def return_minmax_points(self, data):
        
        minl, maxl, minlo, maxlo = 91, -91, 181, -181
        #iterate through all the x,y data points and find if they are smaller/larger than current min/max
        for x, y in zip(data[0], data[1]):
            if (x and y):
                if (minl != 91):
                    if (x < minl):
                        minl = x
                    if (x > maxl):
                        maxl = x
                else:
                    minl, maxl = x, x
                if (minl != 181):
                    if (y < minlo):
                        minlo = y
                    if (y > maxlo):
                        maxlo = y
                else:
                    minlo, maxlo = y, y

        return [minl, maxl, minlo, maxlo]

    def transform_sph_longlat(self, xyz):

        xn, yn, zn = [], [], []
    
        #transform the coordinates from spherical to long and lat
        for (x, y, z) in zip(xyz[0], xyz[1], xyz[2]):
            x_t, y_t, z_t = GFD.cartesian_2_lonlatradius([x,y,z])
            xn.append(x_t)
            yn.append(y_t)
            zn.append(z_t)

        return [xn, yn, zn]

    def parse_pvtu(self):

        print "Parsing pvtu file!"

        path = self.file_dictionary['.pvtu'][0]

        if path:
            #get values of the data types and what they are
            info = vtktools.vtu(path)
            types = info.GetFieldNames()
            #return the xyz value of the data from info
            xyz = [list(t) for t in zip(*info.GetLocations())]
            #apply transformation to these points
            #xyz = self.transform_points(xyz, 32630, 4326)
            xyz = self.transform_sph_longlat(xyz)

            #we have created points 
            if (xyz[0] and xyz[1]):
                self.mesh['Nodes pvtu'] = [xyz[0][0::1000], xyz[1][0::1000]]
                print "-populated node data in dictionary!"
                self.mesh['Boundary conditions'] = self.return_minmax_points([xyz[0], xyz[1]])
                print "-->generated boundary conditions from node data!"
            else:
                print "-failed to populate node data!"

            for t in types:
                self.data[t] = [xyz[0][0::1000], xyz[1][0::1000], tuple(x[0] for x in info.GetField(t))[0::1000]]
                print "-populated "+t+" data!"

        else:
            print "Failed to read data!"

    def return_area(self, values):
        x, y = zip(*values)
        return 0.5*np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))

    def extract_outer_boundary(self):

        values, nodes = [], self.connect_dictionary()

        if nodes:
            #iterate through all the boundayr nodes
            for node in nodes:
                #if node is the start, iterate through all its tags and add their node values to the list
                if node.is_head:
                    #break the loop of nodes (so it's not a closed list)
                    node.previous.next = None
                    node.previous = None                    

                    current_node, curr = node, []
                    while current_node:
                        curr = curr + current_node.nodes
                        current_node = current_node.next
                    values.append(curr)

            #sort the tag values by number of nodes inside them (so [0] is the longest list == outer boundary)
            values.sort(key=lambda x: self.return_area(x), reverse=True)

            #print len(values[1])
            #print values[-1]
            return values

    def connect_dictionary(self):
        
        nodes = []

        if 'Tag dictionary' in self.mesh:
            tag_dictionary = self.mesh['Tag dictionary']

            #print len(tag_dictionary), sum(len(tag_dictionary[x]) for x in tag_dictionary)

            for tag in tag_dictionary:

                #first entry in the tag (first connection)
                first = tag_dictionary[tag][0][2][0]
                #last entry in the tag (last connection)
                last = tag_dictionary[tag][len(tag_dictionary[tag])-1][2][1]
                #assign tag value, first and last points, as well as all the nodes WITHIN the tag
                node = Node(tag, first, last, [n[0] for n in tag_dictionary[tag]])
                nodes.append(node)
        else:
            return

        #iterate through the tags and create connections between them by comparing them to each other

        for node in nodes:
            start, end = node.start, node.end
            for node_compare in nodes:            
                start_n2, end_n2 = node_compare.start, node_compare.end
                #start of the node is connected to the end of the compared node
                if (start == end_n2):
                    node.previous = node_compare
                    node_compare.next = node

                    node.is_head = False
                    
                #end of the current node is connected to the start of the compared node
                elif (end == start_n2):      
                    node.next = node_compare
                    node_compare.previous = node

                    node_compare.is_head = False

                #starts are the same - this is the node that links together the start and end of the tag linked list
                elif (start == start_n2 and node.tag != node_compare.tag):

                    #check if we've already created the connection
                    if node.previous:
                        continue

                    node.previous = node_compare
                    node_compare.next = node

                    node.is_head = False
                    #reverse the node because we attached it to one end, and it goes from start to end (so elements need to be swapped)
                    node_compare.nodes = node_compare.nodes[::-1]

                elif (end == end_n2 and node.tag != node_compare.tag):

                    if node.next:
                        continue

                    node.next = node_compare
                    node_compare.previous = node

                    node_compare.is_head = False
                    
                    #node_compare.nodes = node_compare.nodes[::-1]
                    

        for node in nodes:
            if node.has_head() == False:
                node.is_head = True

        return nodes

    def parse_msh(self):
        
        print "Parsing mesh file!"

        x, y, z, mesh = [], [], [], []
        boundary_bool = False
        tag_dict = {}

        infile = open(self.file_dictionary['.msh'][0], 'r')

        if infile:

            for line in itertools.islice(infile, 5, None):

                if ('$EndNodes' in line):
                    boundary_bool = True

                #if we are not at boundary bools, keep adding nodes
                if (boundary_bool == False):

                    coords = line.split(' ')

                    x_t = coords[1]
                    y_t = coords[2]
                    z_t = coords[3]

                    if (x_t and y_t and z_t):

                        x_t, y_t, z_t = float(x_t), float(y_t), float(z_t)

                        x_t, y_t, z_t = GFD.cartesian_2_lonlatradius([x_t,y_t,z_t])
                        
                        x.append(x_t)
                        y.append(y_t)
                        z.append(z_t)

                else:
                    if ('$EndElements' in line):
                        break

                    if (len(line.split(' ')) > 2):

                        line_sp = line.split(' ')
                    
                        id_line = int(line_sp[1])

                        #check if the line is of a boundary node
                        if (id_line == 1):
                            #nodes are offset by 1 so we have to take it away
                            num = int(line_sp[2])
                            tag = int(line_sp[4])
                            index = int(line_sp[3+num])
                            index2 = int(line_sp[4+num])

                            if (index and index2 and tag):

                                #index starts at 0, but nodes start at 1, so convert them to the right value
                                index, index2 = index-1, index2-1
                                #create tag dictionary of x/y of the two connected nodes, as well as their index
                                if tag in tag_dict:
                                    tag_dict[tag].append([[x[index], y[index]], [x[index2], y[index2]], [index, index2]])
                                else:
                                    tag_dict[tag] = [[[x[index], y[index]], [x[index2], y[index2]], [index, index2]]]

                        #if they are inside boundary values, create the triangles they make up
                        if (id_line == 2):

                            p1_ind, p2_ind, p3_ind = int(line_sp[3+num])-1, int(line_sp[4+num])-1, int(line_sp[5+num])-1            
                            p1, p2, p3 = [x[p1_ind], y[p1_ind]], [x[p2_ind], y[p2_ind]], [x[p3_ind], y[p3_ind]]

                            mesh.append([p1, p2, p3])

        else:
            print "Failed to open mesh file, ending!"
            return

        infile.close()

        if (x and y and z and 'Nodes' not in self.mesh):
            self.mesh['Nodes'] = [x, y, z]
            print "-populated node data in dictionary!"
            self.mesh['Boundary conditions'] = self.return_minmax_points([x, y, z])
            print "-->generated boundary conditions from node data!"
        else:
            print "-failed to populate node data or data already in dictionary!"

        if (len(tag_dict) > 0):
            self.mesh['Tag dictionary'] = tag_dict

            outer = self.extract_outer_boundary()
            if outer:
                self.mesh['Outer boundary'] = outer
                print "-->populated node boundary data with outer boundary!"
            else:
                print "-->failed to populate node boundary data!"

            print "-populated boundary tag data in dictionary!"
        else:
            print "-failed to populate boundary tag nodes!"
        if (len(mesh) > 0):
            self.mesh['Mesh'] = mesh
            print "-populated mesh data in dictionary!"
        else:
            print "-failed to populate mesh dictionary key!"

    def return_mesh_data(self):
        return self.mesh

    def return_data(self):
        return self.data

    def parse_grd(self):
        
        print "Parsing grid file!"

        infile = netcdf.netcdf_file(self.file_dictionary['.grd'][0], 'r')

        x, y, z = infile.variables['x'][::-1], infile.variables['y'][::-1], infile.variables['z'][::-1]

        infile.close()
        
        self.return_matrix_data(x,y,z,'Bathymetry')

    def parse_flml(self):
        print "Parsing flml file!"

        self.flml = XML(self.file_dictionary['.flml'][0])   

    def key_to_correct_parser(self, fileType):
        switcher = {
        #'flml': self.parse_flml,
        #'pvtu': self.parse_pvtu,
        'msh': self.parse_msh,
        'grd': self.parse_grd,
        #'nc': self.parse_nc,
        }

        parser_function = switcher.get(fileType, lambda : 'Unrecognised file type called to parse or file is missing! Skipping.')
        return parser_function()
    
    def parse_all(self):

        if ('.msh' in self.file_dictionary):
            self.key_to_correct_parser('msh')
        if ('.pvtu' in self.file_dictionary):
            self.key_to_correct_parser('pvtu')    
        if ('.nc' in self.file_dictionary):
            self.key_to_correct_parser('nc')
        if ('.grd' in self.file_dictionary):
            self.key_to_correct_parser('grd')
        if ('.flml' in self.file_dictionary):
            self.key_to_correct_parser('flml')
        #for key in self.file_dictionary:
        #    if ('pvtu' not in key and 'msh' not in key):
        #        self.key_to_correct_parser(key)

    def return_matrix_data(self, x, y, z, data_type):
        x_width = len(x)
        y_width = len(y)

        bx, by, bz = [], [], []

        #iterate through the xyz and populate data from the ones we have
        for i in range(0, x_width):
            for j in range(0, y_width):
                if (i%30 == 0 and j%30 == 0):    

                    lon, lat, depth, bounding_mesh = x[i], y[j], z[j][x_width - i - 1], self.mesh['Boundary conditions']                

                    if (lon > bounding_mesh[0] and lon < bounding_mesh[1] and lat > bounding_mesh[2] and lat < bounding_mesh[3]):

                        bx.append(lon)
                        by.append(lat)
                        bz.append(depth)

                        xt = x[i]
                        yt = y[j]

        #print len(bz), len(by), len(bx)
        self.data[data_type] = [bx, by, bz]
        print "-populated "+data_type+" data!"

    def parse_nc(self):

        infile = netcdf.netcdf_file(self.file_dictionary['.nc'][0], 'r')
        h = infile.variables

        x,y,z = h['lon'][::-1], h['lat'][::-1], h['z'][::-1]

        self.return_matrix_data(x, y, z, 'Bathymetry')
