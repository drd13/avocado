#!/usr/bin/env python


"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""


#Object Oriented version of code


import sys,os
if __name__ == "__main__":
    sys.path.append(os.path.realpath(__file__))
from parsers import parserSELAFIN
from osgeo import osr,ogr, gdal
import matplotlib.pyplot as plt
import simplekml
import numpy as np
#sys.path.append('/home/drd13/Documents/Project/Parser/')
import base64

#Summary: nodes in boundary table begin at 1 because when doing 3 in boundaries etc then we obtain the correct answer
#Nodes in IKLE3 begin at 0 because the first connection is between 0,1,2 



def read_file(path):
        with open(path) as f:
                data = f.readlines()
        return data

def read_selafin(path):
        '''read selafinfiles and return:
        [1] the xyz coordinates in a list [[xcoords][ycoords][zcoords]] with xcoords=[xnode1,xnode2...]
        [2] The connectivity table, note that the first node here is node0 unlike in the boundary condition file
        [3] The results at time t with the results being in the form of a list of lists '''
        slfFile = parserSELAFIN.SELAFIN(path)
        coords = [slfFile.MESHX,slfFile.MESHY,slfFile.getVALUES(0)[0]]

        results = {}
        for i in range(len(slfFile.VARNAMES)):
            results[slfFile.VARNAMES[i]] = slfFile.getVALUES(len(slfFile.tags['times'])-1)[i]
        #results takes the form {varname1:list1, varname2:list2}
            
        for i in range(3):
            print(len(coords[i]))
        return coords, slfFile.IKLE3, results

def SaveFigureAsImage(fileName,fig=None,contour=None):
    '''Save figure without padding and margin, this is important to have the figures not be shifted in google earth '''
    a=fig.gca()
    a.set_frame_on(False)
    a.set_xticks([]); a.set_yticks([])
    plt.axis('off')
    fig.savefig(fileName, transparent=True, bbox_inches='tight', \
                        pad_inches=0)
        
    plt.colorbar()
    a.set_visible(False)
    fig.savefig(fileName+'bar', transparent=True, bbox_inches='tight', \
                        pad_inches=0)
    plt.cla() 
    plt.clf()

    #fig.savefig(fileName)
    return

def make_transformer(source_id, target_id=4326):
    '''Create an osr transformer from source_id to target_id with source_id and target_id the ESRI codes for the coordinate systems'''
    source=osr.SpatialReference()
    source.ImportFromEPSG(source_id) #osg
    target=osr.SpatialReference()
    target.ImportFromEPSG(target_id)
    transformer = osr.CoordinateTransformation(source,target)
    return transformer

def convert(x_coord,y_coord, transform):
    '''Convert point from one coordinate system to another'''
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(x_coord, y_coord)
    point.Transform(transform)
    return point.GetPoint() #this is of form (x,y,z(=0))


def get_center(x,y):
    #useful for placing a placemark in the middle of the model
    x_center=sum(x)/float(len(x))
    y_center=sum(y)/float(len(y))
    return [x_center, y_center]



class Simulation():


    '''Creates a kml file from the files used by a telemac simulation'''
    def __init__(self, cli_path, slf_path, cas_path,res_path=None,coord_sys=27700,output_name=None,output_folder=None):
        self.directory = os.path.dirname(os.path.realpath(__file__))
        self.cli_path = cli_path
        self.slf_path = slf_path
        self.cas_path = cas_path
        self.coord_sys = coord_sys
        self.read_files()
        self.create_kml(output_name, output_folder)
        return


               
    def read_files(self):
        if self.cli_path:
            self.cli_data = read_file(self.cli_path)
        if self.cas_path:
            self.cas_data = read_file(self.cas_path)
        #self.elements takes form of [[1],[2],[3]]
        #with 1,2,3 being lists of nodes ex[10,25,35] in each element
        #self.cords=[x,y,z] with x,y,z lists with the first element being the first node
        if self.slf_path:
            self.slf_coords, self.elements, self.results = read_selafin(self.slf_path)

        return

    def create_kml(self,output_name,output_folder):
        self.transformer = make_transformer(self.coord_sys)
        self.kml = simplekml.Kml(open=0, visibility = 1)

        if self.slf_path:
            self.fol = self.kml.newfolder(name='Results')
            self.fol.liststyle.listitemtype = simplekml.ListItemType.radiofolder
            self.create_slf_kml()
        if self.cli_path and self.slf_path:
            self.create_cli_kml()

        print os.path.splitext(os.path.basename(self.slf_path))[0]+'.kml'
        if not output_name:
            output_name=os.path.splitext(os.path.basename(self.slf_path))[0]
        if not output_folder:
            output_folder=os.path.abspath(os.path.join(__file__ ,"../..",'KML'))
            print ('output_folder', output_folder)
        print 'saving to', output_folder + '/' + output_name
        self.kml.savekmz(os.path.join(output_folder, output_name + '.kmz'))    

        return



    def create_cli_kml(self):
        '''finds the boundaries and plots the boundaries then add them to the kml document
        requires a slf file and a cli file '''
        #create functions call the different make functions
        self.boundaries = self.find_cli_boundaries(self.cli_data)
        self.boundary_coords = self.find_boundary_coords(self.slf_coords,self.boundaries)

        self.filtered = self.get_connections()
        self.coordinates = self.get_coordinates(self.filtered)
        self.plot_coordinates(self.coordinates)


        
        counter=0
        for x,y in zip(self.boundary_coords[0],self.boundary_coords[1]):
            if counter<10000:
                counter+=1
                point = self.kml.newpoint(coords = [convert(x,y,self.transformer)], name=self.boundaries[counter-1])
                point.iconstyle.icon.href='http://antigonishoptical.ca/images/black-dot.png'
                point.iconstyle.scale = 0.2
                #kml_boundary = self.kml.newlinestring(name='Mesh Outline',coords = self.projection(self.boundary_coords,self.transformer))
        #coord is temp variable to make code more readable
        coord = convert(float(np.mean(self.boundary_coords[0])), float(np.mean(self.boundary_coords[1])), self.transformer) 
        kml_placemark = self.kml.newpoint( name='Dunbar', description='probably put some parsed info just a test',coords = [coord])
        return

    def create_slf_kml(self):
        extrema = self.draw_results(self.slf_coords,self.results)
        #self.mesh_kml = self.make_kml_mesh()
        self.results_kml = self.make_kml_results(self.results, extrema, self.transformer)
        return

    def find_cli_boundaries(self,cli_data):
        boundaries = []
        for line in cli_data:
            boundaries.append(line.split()[11])
        return boundaries

    def find_boundary_coords(self, slf_coords, boundaries):
        boundary_coords = list(slf_coords)
        for j in range(3):
           boundary_coords[j] = [slf_coords[j][int(i)-1] for i in boundaries] 
        return boundary_coords

    def get_connections(self):
        '''Find which nodes boundary nodes are connected to. This is important in drawing a boundary to the simulation where the inner and outer boundaries ar not'''
        #these functions are meant for mapping and are meant to only be accesible by get connections
        def filter_elements(i):
            #keeps only the triangles on the boundary
            border=False
            for x in i:
                if str(x+1) in self.boundaries:
                    border=True
            return border

        def filter_nodes(i):
            #keeps only the nodes on the boundary
            if str(i+1) in self.boundaries:
                return True
        filtered_IKLE3 = filter(filter_elements, self.elements)
        print('boundary nodes', self.boundaries[0:10])
        print('triangles', len(self.elements))
        print('filtered',filtered_IKLE3[10:60])
        print(len(filtered_IKLE3))
        filtered2_IKLE3 = []
        for i in filtered_IKLE3:
            filtered2_IKLE3.append(filter(filter_nodes,i))
        filtered3_IKLE3 = [i for i in filtered2_IKLE3 if len(i)>1]
        return filtered3_IKLE3


    def get_coordinates(self,filtered):
        coordinates = []
        for i in filtered:
            coordinates.append([[],[]])
            for j in i:
                coordinates[-1][0].append(self.slf_coords[0][int(j)])
                coordinates[-1][1].append(self.slf_coords[1][int(j)])
        print(coordinates[0])
        print(coordinates[1])
        return coordinates


    def plot_coordinates(self,coordinates):
        fol = self.kml.newfolder(name='Outline')
        multi = fol.newmultigeometry(name='Border', visibility = 0)
        for connection in coordinates:
            points = self.projection(connection,self.transformer)
            multi.newlinestring(coords=points)
        return


    def draw_results(self,slf_coords,results):
        extrema=[]
        for key in results:
            contour = plt.tricontourf(slf_coords[0],slf_coords[1],results[key])
            extrema.append([plt.xlim(), plt.ylim()])
            #extrema of form [[extremaVAR1plot],[extremaVAR2plot]...] with extremaVAR1plot of form [[xmin,xmax],[ymin,ymax]]
            print('saving as:', os.path.abspath(os.path.join(__file__ ,"../..",'Images',key)))
            SaveFigureAsImage(os.path.abspath(os.path.join(__file__ ,"../..","Images", base64.urlsafe_b64encode(key) )),plt.gcf(), contour)
        return extrema
            
        
    def projection(self, coords, transformer):
        '''Converts between the original coordinate system of the simulation and long/lat
        coords is of form [[xList],[yList]] with xList=[x1,x2,x3...]'''
        ring = ogr.Geometry(ogr.wkbLinearRing)
        for x,y  in zip(coords[0],coords[1]):
            ring.AddPoint(x,y)
        ring.Transform(transformer)
        return ring.GetPoints()

    def get_coord_elements(self,slf_coords):
        '''takes the list of elements in the form [element1]=[node1,node2,node3] and converts it to the form [x1,x2,x3],[y1,..]'''
        element_coord = []
        #first node is node 0
        for element in self.elements:
            element_coord.append([[],[]])
            for node in element:
                element_coord[-1][0].append( slf_coords[0][node])
                element_coord[-1][1].append( slf_coords[1][node])
        #element_coordinates of form [[element1], [element2], [element3]...] with element1=[[x1,x2,x3],[y1,y2,y3]]
        return element_coord


    def get_corners(self,extrema, transformer):
        '''find the boundaries of the plots and converts them to long/lat this is used when created ground overlays'''
        #extrema might need a bit of changing since the addition of multiple vars for plotting
        minimum = convert(extrema[0][0],extrema[1][0],transformer)
        maximum = convert(extrema[0][1],extrema[1][1],transformer)
        return [maximum[1],minimum[1],maximum[0],minimum[0]]

    def make_kml_mesh(self):
        '''creates the kml multigeometry associated with the actual mesh'''
        element_coord = self.get_coord_elements(self.slf_coords)
        mesh_kml = self.kml.newmultigeometry(name='Mesh Geometry')
        x=([z for element in element_coord for z in element[0]])
        y=([z for element in element_coord for z in element[1]])
        corner = self.get_corners([[min(x),max(x)],[min(y),max(y)]],self.transformer)
        mesh_kml.region.lod = simplekml.Lod(minlodpixels=1024,maxlodpixels=-1, minfadeextent=20, maxfadeextent=50)
        mesh_kml.region.latlonaltbox.north = corner[0]
        mesh_kml.region.latlonaltbox.south = corner[1]
        mesh_kml.region.latlonaltbox.east = corner[2]
        mesh_kml.region.latlonaltbox.west = corner[3]
        for element in element_coord:
            mesh_kml.newlinestring(coords=self.projection(element,self.transformer))
        return mesh_kml

    def make_kml_results(self,results,extrema,transformer):
        #results_kml is of form [obj1,obj2...] with obj1 being simplekml groundoverlayobj
        results_kml = []
        i=0
        for key in results:
            temp = extrema
            #self.extrema i wont work as dictionairy
            result = self.fol.newdocument(name = key)
            print('extrena',temp[i])
            corners = self.get_corners(temp[i],transformer)
            print('corners',corners)
            #results_kml.append(self.fol.newgroundoverlay(name=key, color = '7fffffff'))
            #temporary structure            
            overlay = result.newscreenoverlay(name=key,color = '7fffffff' )
            overlay.icon.href = os.path.abspath(os.path.join(__file__ ,"../..",'Images',base64.urlsafe_b64encode(key)+'bar.png'))
            overlay.screenxy = simplekml.ScreenXY(x=0.05,y=0.99,xunits=simplekml.Units.fraction,
                                     yunits=simplekml.Units.fraction)
            overlay.overlayxy = simplekml.OverlayXY(x=0.05,y=0.99,xunits=simplekml.Units.fraction,
                                     yunits=simplekml.Units.fraction)

            overlay.region.lod = simplekml.Lod(minlodpixels=1024,maxlodpixels=-1, minfadeextent=20, maxfadeextent=50)
            overlay.region.latlonaltbox.north = corners[0]
            overlay.region.latlonaltbox.south = corners[1]
            overlay.region.latlonaltbox.east = corners[2]
            overlay.region.latlonaltbox.west = corners[3]

            groundOverlay = result.newgroundoverlay(name=key, color = '7fffffff')
            groundOverlay.icon.href = os.path.abspath(os.path.join(__file__ ,"../..",'Images',base64.urlsafe_b64encode(key)+'.png'))
            groundOverlay.latlonbox.north = corners[0]
            groundOverlay.latlonbox.south = corners[1] 
            groundOverlay.latlonbox.east = corners[2]
            groundOverlay.latlonbox.west = corners[3]
            print('retrieving as:', os.path.abspath(os.path.join(__file__ ,"../..",'Images',key+'.png')))
            i+=1
        return results_kml        
        



#sim=Simulation('/home/drd13/Downloads/Inverness/Result2D_Inverness_BC.cli','/home/drd13/Downloads/Inverness/Result2D_Inverness.slf','/home/drd13/Documents/Project/Simulation/telemac_event_08.cas',27700)
                

