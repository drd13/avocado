#!/usr/bin/env python


"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""

import sqlite3
import os

class database():
    def __init__(self, fields, name='parameters'):
        '''creates a SQL database using sqlit3:
        - name is the name of the table, fields is a list with each element representing one column of the database.
        - Whitespace is converted to _ to avoid complicated sql statements'''
        self.columns = len(fields)
        self.name = 'parameters'
        fields = [field.replace(' ','_') for field in fields]
        try:
            db = sqlite3.connect('/home/drd13/Documents/GUI/Scripts/database.db')

            cursor = db.cursor()
            #cursor.execute('''CREATE TABLE IF NOT EXISTS
            #          simulations(software TEXT PRIMARY KEY, author TEXT, location TEXT, filesize INTEGER) ''')
            cursor.execute('''CREATE TABLE IF NOT EXISTS '''+ name + ''' (''' + ' TEXT, '.join(fields)+'''TEXT)''')
            print('now commiting')
            db.commit()
        except Exception as e:
            db.rollback()
            raise e

        finally:
            db.close()


    def add_row(self, data, name= None):
        '''add a row to the table (with a row representing a simulation) with data: a list of the inputs for the table''' 
        if not name:
            name = self.name	
        print('data is :', data)
        sql = "INSERT INTO "+name+" VALUES ("+','.join(['?']*self.columns) + ")"
        data = [datum.replace(' ','_') for datum in data]
        try:
            db = sqlite3.connect('/home/drd13/Documents/GUI/Scripts/database.db')
            cursor = db.cursor()
            print('inserting data')
            cursor.execute(sql, data)

            db.commit()
        except Exception as e:
            db.rollback()
            raise 

        finally:
            db.close()


    def query(self, field):
        '''Display all the inputs for a specific field where field is the name of the variable with spaces replaced with _'''
        field = field.replace(' ','_')
        try:
            db=sqlite3.connect('/home/drd13/Documents/GUI/Scripts/database.db')
            cursor = db.cursor()
            #cursor.execute('''select (?) from simulations''', (field,))
            sql = "select %s from " + self.name        
            cursor.execute(sql % field)    
            print (cursor.fetchall())

        except Exception as e:
            db.rollback()
            raise
        finally:
            db.close()

#update([('fluidity','jan', 'some boring region in the mediterannian', '50')])
#query('location')
    

