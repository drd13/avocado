#!/usr/bin/env python

"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys, os

script_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),'..','Scripts')))
if script_subfolder not in sys.path:
    sys.path.insert(0, script_subfolder)


import parserSELAFIN
from osgeo import osr,ogr, gdal
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import simplekml
import numpy as np
import base64
import tkMessageBox
import time

#Code for ParserTelemac rewritten

class FileHandler():


    def read_file(self,path):
        try:
            with open(path) as f:
                data = f.readlines()
            return data
        except:
            tkMessageBox.showerror('Error', 'Something went wrong in opening ' +  path)

    def read_selafin(self, path):
        '''read selafinfiles and return:
        [1] the xyz coordinates in a list [[xcoords][ycoords][zcoords]] with xcoords=[xnode1,xnode2...]
        [2] The connectivity table, note that the first node here is node0 unlike in the boundary condition file
        [3] The results at time t with the results being in the form of a list of lists '''
        try:
            parameters = {}
            slfFile = parserSELAFIN.SELAFIN(path)
            coords = [slfFile.MESHX,slfFile.MESHY]

            for i in range(len(slfFile.VARNAMES)):
                parameters[slfFile.VARNAMES[i]] = slfFile.getVALUES(len(slfFile.tags['times'])-1)[i]
            #results takes the form {varname1:list1, varname2:list2}
            return coords, slfFile.IKLE3, parameters
        except:
            tkMessageBox.showerror('Error', 'Something went wrong in opening the selafin file at' + str(path) )

    def plot_figure(self, xy, z, name):
        #problem was that the coordinate system was wrong so the changes were minute
        self.xy= xy
        self.z=z
        contour = plt.tricontourf(xy[0][::3],xy[1][::3],z[::3])
        minmax = {'xmin':min(xy[0]),
                   'xmax':max(xy[0]),
                   'ymin':min(xy[1]),
                   'ymax':max(xy[1])}
        extrema = [[minmax['xmin'], minmax['ymin']], [minmax['xmax'], minmax['ymin']] ,[minmax['xmax'], minmax['ymax']], [minmax['xmin'], minmax['ymax']]]
        self.save_figure(os.path.abspath(os.path.join(__file__ ,"../..","Images", name )),plt.gcf(), minmax)
        return extrema

    def save_figure(self, name, fig=None, minmax=None):
        '''Save figure without padding and margin, this is important to have the figures not be shifted in google earth '''
        a=fig.gca()
        a.set_xlim([minmax['xmin'], minmax['xmax']])
        a.set_ylim([minmax['ymin'], minmax['ymax']])


        a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('off')
        fig.savefig(name, transparent=False, bbox_inches='tight', pad_inches=0)

        plt.colorbar()
        a.set_visible(False)
        fig.savefig(name+'bar', transparent=True, bbox_inches='tight', pad_inches=0)

        time.sleep(5)
        plt.cla()
        plt.clf()
        return




    def crop_figure(self, name, boundary):
        path = os.path.abspath(os.path.join(__file__ ,"../..",'Images',name+'.png'))
        figure = np.asarray(Image.open(path).convert('RGBA'))
        mask_image = Image.new('L', (figure.shape[1], figure.shape[0]), 0)
        ImageDraw.Draw(mask_image).polygon(boundary[0], outline = 1, fill = 1)
        for inner_border in boundary[1:]:
            ImageDraw.Draw(mask_image).polygon(inner_border, outline = 0, fill = 0)
        mask = np.array(mask_image)
        new_image_array = np.empty(figure.shape,dtype='uint8')
        new_image_array[:,:,:3] = figure[:,:,:3]
        new_image_array[:,:,3] = mask*255
        cropped_figure = Image.fromarray(new_image_array, 'RGBA')
        cropped_figure.save( os.path.abspath(os.path.join(__file__ ,"../..",'Images',name+'2'+'.png'))  )
        return


    def transform_to_pixels(self, item, width_ratio, height_ratio,xmin, ymin, ymax):
        #need a tuple for the coordinates to work might need to polish the recursion to avoid bugs
        if ( len(item)==2 or len(item)==3 ) and isinstance(item[0], float):
            x=(item[0]-xmin)*width_ratio
            y=(ymax-ymin)*height_ratio - (item[1]-ymin)*height_ratio
            return (x, y)
        else:
            return tuple(self.transform_to_pixels(sub_item, height_ratio, width_ratio, xmin, ymin, ymax) for sub_item in item)

    def setup_cropping(self, corners, key, polygon_coords):
        path = os.path.abspath(os.path.join(__file__ ,"../..",'Images',base64.urlsafe_b64encode(key)+'.png'))
        width, height = Image.open(path).size
        width_ratio = width/(corners[2][0]-corners[0][0])
        height_ratio = height / (corners[2][1]-corners[0][1])
        polygon_pixels = self.transform_to_pixels(polygon_coords, width_ratio, height_ratio, corners[0][0], corners[0][1], corners[2][1])
        #name is encoded so as to avoid clashes with url
        self.crop_figure(base64.urlsafe_b64encode(key), polygon_pixels)
        return

    def get_width_from_height(self,path, desired_height = 450.):
        width, height = Image.open(path).size
        ratio = float(height)/desired_height
        width = int(ratio*width)
        return width, height


class Simulation(FileHandler):

    def __init__(self, cli_path, slf_path, cas_path=None, res_path = None, coord_sys=27700, Name=None):
        self.directory = os.path.dirname( os.path.realpath(__file__) )
        self.kml = KML(source_id=coord_sys)
        polygon_coords = self.create_boundary(cli_path,slf_path)
        self.create_figures(slf_path, polygon_coords)


        x = (max(polygon_coords[0], key=lambda x: x[0])[0] + min(polygon_coords[0], key=lambda x: x[0])[0] )/2.
        y = (max(polygon_coords[0], key=lambda x: x[1])[1] + min(polygon_coords[0], key=lambda x: x[1])[1] )/2.
        self.kml.draw_placemark(coords=[[x,y]], name=Name)
        self.kml_path = self.kml.save_kml(name=Name)

    def create_boundary(self, cli_path, slf_path):
        #put in external module instead
        cli_data = self.read_file(cli_path)
        node_coords, mesh_triangles, parameters = self.read_selafin(slf_path)
        boundary_nodes = self.extract_boundary_nodes(cli_data)
        boundary_edges = self.get_boundary_edges(mesh_triangles, boundary_nodes)
        polygon_nodes = self.createPolygon(boundary_edges, boundary_nodes)
        polygon_coords = self.transform_to_coords(polygon_nodes, node_coords)
        self.kml.draw_linestrings(polygon_coords)
        return polygon_coords

    def extract_boundary_nodes(self, cli_data):
        boundaries = []
        for line in cli_data:
           #the boundary table starts at 1 so substract 1 to match mesh table
            boundaries.append(int(line.split()[11]) - 1 )
        return boundaries

    def transform_to_coords(self,item,coords):
        if isinstance(item, int):
            return [coords[0][item], coords[1][item]]
        else:
            return [self.transform_to_coords(item2, coords) for item2 in item]

    def get_boundary_edges(self, mesh_triangles, boundary_nodes):
        def is_border_triangle(element):
            #keeps only the triangles on the boundary
            on_border=False
            for node in element:
                if int(node) in boundary_nodes:
                    on_border=True
            return on_border
        def is_border_node(node):
            #keeps only the nodes on the boundary
            if node in boundary_nodes:
                return True
        def get_edge_combinations(triangle):
            if len(triangle)==1:
                return (None,)
            if len(triangle)==2:
                return ([triangle[0],triangle[1]],)
            if len(triangle)==3:
                return [triangle[0],triangle[1]], [triangle[0],triangle[2]], [triangle[1],triangle[2]]

        boundary_triangles = filter(is_border_triangle, mesh_triangles)
        #returns only the boundary nodes within the triangles
        boundary_nodes = [filter(is_border_node, triangle) for triangle in boundary_triangles]
        #remove triangles that only have 1 node
        boundary_edges = map(get_edge_combinations, boundary_nodes)
        boundary_edges = [y for x in map(get_edge_combinations, boundary_nodes) for y in x if y is not None]
        #edge borders will only appear once, inner triangles will appear multiple times. This allows to only keep the border triangles
        boundary_edges = [edge for edge in boundary_edges if boundary_edges.count([edge[1],edge[0]])+boundary_edges.count([edge[0],edge[1]]) == 1]
        return boundary_edges

    def create_figures(self, slf_path, polygon_coords):
        node_coords, mesh_triangles, parameters = self.read_selafin(slf_path)
        coords = []
        for x in range(len(node_coords[0])):
            #required for running the projection
            coords.append([node_coords[0][x],node_coords[1][x]])

        folder = self.kml.make_folder(name = 'Results')
        for key in parameters:
            self.kml.draw_plot(coords, parameters[key], key, polygon_coords, folder)
        return

    def create_figure(self, parameters, extrema, key, polygon_coords):
            path = os.path.abspath(os.path.join(__file__ ,"../..",'Images',base64.urlsafe_b64encode(key)+'.png'))
            width, height = Image.open(open(path)).size
            width_ratio = width /(extrema[key][1][0]-extrema[key][0][0])
            height_ratio = height / (extrema[key][1][1]-extrema[key][0][1])
            polygon_pixels = self.transform_to_pixels(polygon_coords, width_ratio, height_ratio, extrema[key][0][0], extrema[key][0][1], extrema[key][1][1])
            self.crop_figure(key, polygon_pixels)
            self.kml.draw_plot(extrema[key], key)
            return

    def createPolygon(self, edges, boundary_nodes):
        for node in boundary_nodes:
            indexes = self.edges_with_node(node, edges)
            if len(indexes) == 2:
                indexes.sort()
                if edges[indexes[0]][0]==node:
                    if edges[indexes[1]][0]==node:
                        #both have common node on left so must reverse one
                        edges[indexes[0]].reverse()
                        new_edge = list(edges[indexes[0]][:-1]) + [node] + list(edges[indexes[1]][1:])
                        edges.append(new_edge)
                        edges.pop(indexes[1])
                        edges.pop(indexes[0])
                    elif edges[indexes[1]][-1] == node:
                        #index[0] has common node on left  index[1] on right
                        new_edge = list(edges[indexes[1]][:-1]) + [node] + list(edges[indexes[0]][1:])
                        edges.append(new_edge)
                        edges.pop(indexes[1])
                        edges.pop(indexes[0])
                    else:
                        print('something went wrong')
                if edges[indexes[0]][-1]==node:
                    if edges[indexes[1]][-1]==node:
                        #both have common node on right so must reverse one
                        edges[indexes[0]].reverse()
                        new_edge = list(edges[indexes[1]][:-1]) + [node] + list(edges[indexes[0]][1:])
                        edges.append(new_edge)
                        edges.pop(indexes[1])
                        edges.pop(indexes[0])
                    elif edges[indexes[1]][0]==node:
                        new_edge = list(edges[indexes[0]][:-1]) +[node] + list(edges[indexes[1]][1:])
                        edges.append(new_edge)
                        edges.pop(indexes[1])
                        edges.pop(indexes[0])
        return edges

    def edges_with_node(self, node, edges):
        return [index for index, edge in  enumerate(edges) if node in edge]


class KML(FileHandler):
    def __init__(self,source_id=27700, target_id=4326):
        self.document = simplekml.Kml(open=0, visibility = 1)
        self.transformer = self.make_transformer(source_id,target_id)

    def save_kml(self, name=None, folder=None):

        if not name:
            name = 'bath2'
        if not folder:
            folder=os.path.abspath(os.path.join(__file__ ,"../..",'KML'))
            print ('output_folder', folder)

        path = os.path.join(folder, name + '.kmz')
        print 'saving to', path

        self.document.savekmz(path)
        return path

    def make_transformer(self,source_id, target_id):
        '''Create an osr transformer from source_id to target_id with source_id and target_id the ESRI codes for the coordinate systems'''
        source=osr.SpatialReference()
        source.ImportFromEPSG(source_id)
        target=osr.SpatialReference()
        target.ImportFromEPSG(target_id)
        transformer = osr.CoordinateTransformation(source,target)
        return transformer

    def project_coordinates(self, coords, transformer=None):
        '''projects recursively the list given'''

        def populate_ring(item,ring):
            '''Assumes data is of form '[x1,y1],[x2,y2] with optional nesting'''
            if ( len(item)==2 or len(item)==3 ) and isinstance(item[0], float):
                ring.AddPoint(item[0], item[1])
            else:
                [populate_ring(item2,ring) for item2 in item]

        if not transformer:
            transformer = self.transformer

        ring = ogr.Geometry(ogr.wkbLinearRing)
        populate_ring(coords,ring)
        ring.Transform(transformer)

        return ring.GetPoints()

    def draw_plot(self, xy, z, key, polygon_coords, container=None):
        if not container:
            container = self.document

        xy = self.project_coordinates(xy)
        polygon_coords = [self.project_coordinates(polygon) for polygon in polygon_coords]
        coords = [[],[]]
        for n in range(len(xy)):
            coords[0].append(xy[n][0])
            coords[1].append(xy[n][1])


        corners = self.plot_figure(coords, z, base64.urlsafe_b64encode(key))

        plot = container.newdocument(name = key)
        self.setup_cropping(corners, key, polygon_coords)
        path_legend = os.path.abspath(os.path.join(__file__ ,"../..",'Images',base64.urlsafe_b64encode(key)+'bar.png'))
        w,h = self.get_width_from_height(path_legend)


        legend = plot.newscreenoverlay(name = key,color = '7fffffff' )
        legend.icon.href = path_legend
        legend.screenxy = simplekml.ScreenXY(x=0.05,y=0.99,xunits=simplekml.Units.fraction,yunits=simplekml.Units.fraction)
        legend.overlayxy = simplekml.OverlayXY(x=0.05,y=0.99,xunits=simplekml.Units.fraction,yunits=simplekml.Units.fraction)
        legend.size.x = w
        legend.size.y = h

        #region makes the overlay disapeer when zoomed out
        legend.region.lod = simplekml.Lod(minlodpixels=1024,maxlodpixels=-1, minfadeextent=20, maxfadeextent=50)
        legend.region.latlonaltbox.east = corners[2][0]
        legend.region.latlonaltbox.west = corners[0][0]
        legend.region.latlonaltbox.south = corners[0][1]
        legend.region.latlonaltbox.north = corners[2][1]

        figure = plot.newgroundoverlay(name=key, color = '7fffffff')
        print('path towards plot', os.path.abspath(os.path.join(__file__ ,"../..",'Images',base64.urlsafe_b64encode(key)+'2'+'.png')))
        figure.icon.href = os.path.abspath(os.path.join(__file__ ,"../..",'Images',base64.urlsafe_b64encode(key)+'2'+'.png'))
        figure.gxlatlonquad.coords = corners

    def make_folder(self, name, radio = True, container=None):
        if not container:
            container = self.document
        folder = container.newfolder(name = name)
        if radio:
            folder.liststyle.listitemtype = simplekml.ListItemType.radiofolder
        return folder

    def draw_linestrings(self, edges, container = None):
        if not container:
            container = self.document
        folder = container.newfolder(name='Outline')
        geometry = folder.newmultigeometry(name='Border', visibility = 0)
        for edge in edges:
            converted_coords = self.project_coordinates(edge)
            geometry.newlinestring(coords = converted_coords)
        return

    def draw_placemark(self, coords, name = None, container = None):
        if not container:
            container = self.document
        if name:
            container.newpoint(name=name, coords = self.project_coordinates(coords) )
        else:
            container.newpoint(coords = self.project_coordinates(coords))
        return





'''
#unit tests    
a=BoundaryEdges(1,2)        
b = a.remove_duplicates([[55,4],[2,4],[3,6],[2,8],[4,3]],[[4,3],[5,6],[3,6]])
b,c= a.find_duplicates([1,2,44,55,6,44,1,5])
b = edges_with_node(1, [[1,55],[2,44],[1,33],[2,88]])
print(b)
'''


def investigation_coordinates(coords = [40.0,55.6]):
    kml = KML()
    kml.draw_placemark(name='original', coords = [coords])
    transformer1 = kml.make_transformer(27700, 4326)
    transformer2 = kml.make_transformer(4326, 27700)
    print(coords)
    print('1')
    coords = kml.project_coordinates(coords, transformer1)
    print(coords)
    print('2')
    coords = kml.project_coordinates(coords, transformer2)
    print(coords)
    print('3')
    kml.draw_placemark(name='transformer', coords = coords)
    kml.save_kml('stuff')
    return kml



#sim=Simulation('/home/drd13/Downloads/Inverness/Result2D_Inverness_BC.cli','/home/drd13/Downloads/Inverness/Result2D_Inverness.slf','/home/drd13/Documents/Project/Simulation/telemac_event_08.cas',27700)

#sim=Simulation('/home/drd13/Documents/Project/Simulation/boundaries_out.cli','/home/drd13/Documents/Project/Simulation/bathy2_out.sel','/home/drd13/Documents/Project/Simulation/telemac_event_08.cas',27700)

#remember to modify the way extrema is being used

'''if __name = '__main__':
       #sim=Simulation('/home/drd13/Downloads/Inverness/Result2D_Inverness_BC.cli','/home/drd13/Downloads/Inverness/Result2D_Inverness.slf','/home/drd13/Documents/Project/Simulation/telemac_event_08.cas',27700)

       sim=Simulation('/home/drd13/Downloads/Inverness/Result2D_Inverness_BC.cli','/home/drd13/Downloads/Inverness/Result2D_Inverness.slf','/home/drd13/Documents/Project/Simulation/telemac_event_08.cas',27700)
'''


