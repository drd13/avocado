#!/usr/bin/env python

"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""


import sys
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),'Scripts')))
if cmd_subfolder not in sys.path:
    sys.path.insert(0, cmd_subfolder)

try:
    import Tkinter
except ImportError:
    print 'ERROR: No module named tkinter, please install the python-tk package'
    sys.exit(1)
import tkFileDialog
import tkMessageBox
import ConfigParser

try:
    from PIL import Image
except ImportError:
    print 'WARNING: could not import PIL!'

try:
    import git
except ImportError:
    print 'WARNING: could not import the git module!'

try:
    from Avocado import ParserTelemac
    from Avocado import Telemac
except ImportError:
    print 'WARNING: could not import Telemac modules!'

try:
    from Avocado import visualiser
except ImportError:
    print 'WARNING: could not import Fluidity modules!'

try:
    from Avocado import db
except ImportError:
    print 'WARNING: could not load database module - database functions will not work!'


try:
    import shutil
except ImportError:
    print 'WARNING: could not load the shutil module'

class pyRDM_GUI(Tkinter.Tk):
    def __init__(self,parent):
        Tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()
        self.addMenus()
        self.extensions = {'configurationExtensions':['.cas'], 'meshExtensions':['.sel','.slf', '.msh'], 'inputExtensions' :['.cli','.pvtu', '.grd'], 'outputExtensions' : ['.res'] }
        self.telemacExtensions = ['.cas','.sel','.res','.slf','.cli']
        self.fluidityExtensions = ['.pvtu','.pvu','.msh','.grd']
        self.kml = None

        #List of files in each category



    def initialize(self):
        self.grid() #Outer grid for packing widgets


        #testing to see if this solves issue
        self.versionVariable = Tkinter.StringVar()


        logo_path =os.path.realpath(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),'Images','logo.gif')))
        logo = Tkinter.PhotoImage(file=logo_path)
        self.logo = Tkinter.Label(self, image=logo)
        self.logo.image = logo
        self.logo.place(x=685, y=120)


        
        ## Software name
        self.label = Tkinter.Label(self, text="Name")
        self.label.grid(column=0,row=0, sticky='w', padx=5, pady=5)
        self.nameVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.nameVariable, width = 50)
        self.entry.bind("<Enter>", self.onEnterName)
        self.entry.bind("<Leave>", self.onLeave)
        self.entry.grid(column=1,row=0, columnspan=2,sticky='w')


        #Publication Method
        self.label = Tkinter.Label(self, text="Publication Method:")
        self.label.grid(column=3,row=0, sticky='w', padx=5, pady=5)
        self.publicationMethod = Tkinter.StringVar()
        self.publicationMethod.set("Locally")  # initial value
        self.publicationMenu = Tkinter.OptionMenu(self, self.publicationMethod,'FigShare', 'SSH', 'Locally',command=self.onCheckPublication)
        self.publicationMenu.grid(column=4,row=0, sticky='we', padx=5, pady=5)

        #Software direcory
        ## Git directory location
        self.label = Tkinter.Label(self, text="Software directory")
        self.label.grid(column=0,row=1, sticky='w', padx=5, pady=5)
        self.gitDirectory = Tkinter.StringVar()
        self.gitentry = Tkinter.Entry(self, textvariable=self.gitDirectory, state='disabled', width = 50)
        self.gitentry.bind("<Enter>", self.onEnterSource)
        self.gitentry.bind("<Leave>", self.onLeave)
        self.gitentry.grid(column=1,row=1, columnspan=2,sticky='w')
        self.gitbutton = Tkinter.Button(self,text=u'Find location', command=self.mGitOpen, width = 10, state='disabled')
        self.gitbutton.grid(column=3,row=1, sticky='w', padx=5, pady=5)
        ## Check button to turn git field on/off for publishing all software or just data
        self.turn = Tkinter.IntVar()
        self.chk = Tkinter.Checkbutton(self, text = 'Add software', variable = self.turn, command=self.turn_check)
        self.chk.grid(column=4, row=1, sticky='w', padx=5, pady=5)

        #Transition To seperate the two types of inputs
        self.emptyFrame = Tkinter.Frame(self, height = 3, border=1, relief=Tkinter.RIDGE)
        self.emptyFrame.grid(column=0, columnspan=5, row=2, sticky='we')

        ## Simulation directory
        self.label = Tkinter.Label(self, text="Simulation Directory")
        self.label.grid(column=0,row=3, sticky='w', padx=5, pady=5)
        self.sourceVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.sourceVariable, width = 50)
        self.entry.bind("<Enter>", self.onEnterSource)
        self.entry.bind("<Leave>", self.onLeave)
        self.entry.grid(column=1,row=3,columnspan=2, sticky='w')
        self.button = Tkinter.Button(self,text=u'Find location', command=self.mOpenDirectory, width = 10)
        self.button.grid(column=3,row=3, sticky='w', padx=5, pady=5)
        self.button = Tkinter.Button(self,text=u'Autopopulate', command=self.mAutopopulate, width = 10)
        self.button.grid(column=4,row=3, sticky='w', padx = 5, pady = 5)


        ## Configure file location
        self.label = Tkinter.Label(self, text="Configuration file")
        self.label.grid(column=0,row=4, sticky='w', padx=5, pady=5)
        self.configurationVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.configurationVariable, width = 50)
        self.entry.bind("<Enter>", self.onEnterConfiguration)
        self.entry.bind("<Leave>", self.onLeave)
        self.entry.grid(column=1,row=4, columnspan=2, sticky='w')
        self.button = Tkinter.Button(self,text=u'Find location', command=self.mOpenConfiguration, width = 10)
        self.button.grid(column=3,row=4, sticky='w', padx=5, pady=5)

        ## Output file location
        self.label = Tkinter.Label(self, text="Output data")
        self.label.grid(column=0,row=5, sticky='w', padx=5, pady=5)
        self.outputVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.outputVariable, width = 50)
        self.entry.bind("<Enter>", self.onEnterOutput)
        self.entry.bind("<Leave>", self.onLeave)
        self.entry.grid(column=1, row=5, columnspan=2, sticky='w')
        self.button = Tkinter.Button(self,text=u'Find location', command=self.mOpenOutput, width = 10)
        self.button.grid(column=3,row=5, sticky='w', padx=5, pady=5)

        ## Input file location
        self.label = Tkinter.Label(self, text="Input data")
        self.label.grid(column=0,row=6, sticky='w', padx=5, pady=5)
        self.inputVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.inputVariable, width = 50)
        self.entry.bind("<Enter>", self.onEnterInput)
        self.entry.bind("<Leave>", self.onLeave)
        self.entry.grid(column=1,row=6, columnspan=2, sticky='w')
        self.button = Tkinter.Button(self,text=u'Find location', command=self.mOpenInput, width = 10)
        self.button.grid(column=3,row=6, sticky='w', padx=5, pady=5)
        self.button = Tkinter.Button(self,text=u'Clear fields', command=self.onClear, width = 10)
        self.button.grid(column=4,row=6, sticky='w', padx = 5, pady = 5)

        ## Mesh file location
        self.label = Tkinter.Label(self, text="Mesh file")
        self.label.grid(column=0,row=7, sticky='w', padx=5, pady=5)
        self.meshVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.meshVariable, width = 50)
        self.entry.bind("<Enter>", self.onEnterMesh)
        self.entry.bind("<Leave>", self.onLeave)
        self.entry.grid(column=1,row=7, columnspan=2, sticky='w')
        self.button = Tkinter.Button(self,text=u'Find location', command=self.mOpenMesh, width = 10)
        self.button.grid(column=3,row=7, sticky='w', padx=5, pady=5)
        self.button = Tkinter.Button(self,text=u'Publish!', command=self.OnButtonClick, width = 10)
        self.button.grid(column = 4, row = 7, sticky = 'w', padx=5, pady=5)




        #Coordinate system
        self.label = Tkinter.Label(self, text="Coord system:")
        self.label.grid(column=3,row=8, sticky='w', padx=5, pady=5)
        self.coordVariable = Tkinter.IntVar()
        self.entry = Tkinter.Entry(self, textvariable=self.coordVariable, width = 13)
        self.entry.grid(column=4,row=8, columnspan=2,sticky='w', padx=5, pady=5)

        ## Private checkbox and publish button
        self.privacyInt = Tkinter.IntVar()
        self.privacyCheck = Tkinter.Checkbutton(self, text="Private",
                                                variable=self.privacyInt,
                                                command=self.onPrivacyCheck,state='disabled')
        self.privacyCheck.select()
        self.privacyCheck.grid(column=0,row=8)

        ## Status bar
        self.labelVariable = Tkinter.StringVar()
        label = Tkinter.Label(self, textvariable=self.labelVariable,
                              anchor='w')
        label.grid(column=1, row=8, columnspan=3, sticky='w', padx=5)
        self.labelVariable.set(u"Status: Unpublished")


        ## Help bar
        self.helpVariable = Tkinter.StringVar()
        label = Tkinter.Label(self,textvariable=self.helpVariable,
                              anchor='w')
        label.grid(column=0,row=8,columnspan=3, sticky='sw')



        self.resizable(False,False)

    def onCheckPublication(self, publicationMethod):
        if publicationMethod== 'FigShare':
            self.privacyCheck.configure(state='normal')
        else:
            self.privacyCheck.configure(state='disabled')

    def turn_check(self):
        if self.turn.get():
            self.gitentry.configure(state='normal')
            self.gitbutton.configure(state='normal')
        else:
            self.gitentry.configure(state='disabled')
            self.gitbutton.configure(state='disabled')
            self.gitDirectory.set('')

    def onLeave(self,event):
        self.helpVariable.set(u"")

    def onEnterName(self,event):
        if self.nameVariable.get() == '':
            self.helpVariable.set(u"Enter name or location of simulation")

    def onEnterSource(self,event):
        if self.sourceVariable.get() == '':
            self.helpVariable.set(u"Enter the directory containing your software, this directory must be a git directory")

    def onEnterVersion(self,event):
        if self.versionVariable.get() == '':
            self.helpVariable.set(u"Enter version number, or attempt to fetch automatically")

    def onEnterConfiguration(self,event):
        if self.configurationVariable.get() == '':
            self.helpVariable.set(u"Enter location of configuration file (Optional)")

    def onEnterMesh(self,event):
        if self.meshVariable.get() == '':
            self.helpVariable.set(u"Enter location of mesh file (Optional)")

    def onEnterInput(self,event):
        if self.inputVariable.get() == '':
            self.helpVariable.set(u"Enter location of input data (Optional)")

    def onEnterOutput(self,event):
        if self.outputVariable.get() == '':
            self.helpVariable.set(u"Enter location of output data (Optional)")

    def onEnterPublish(self,event):
        textVar=''
        counter=0
        directory = self.sourceVariable.get()
        file_dict = self.check_Files(directory)
        if file_dict:
            #print('file dict is ', file_dict)
            if '.sel' in file_dict or '.slf' in file_dict:
                textVar='Directory containing telemac simulation with files:'
            else:
                textVar='Directory does not contain adequate files:'
            for key in file_dict:
                if counter < 5:
                    textVar+= '   '+key
                elif counter == 5:
                    textVar += '...'
                counter +=1
        else: 
            textVar='Please Select a directory'
        
        self.helpVariable.set(unicode(textVar, "utf-8"))
    def onLeavePublish(self,event):
        self.helpVariable.set(u"")

    def onClear(self):

        self.configurationVariable.set('')
        self.inputVariable.set('')
        self.outputVariable.set('')
        self.meshVariable.set('')

    def addMenus(self):
        menubar = Tkinter.Menu(self)
        filemenu = Tkinter.Menu(menubar, tearoff = 0)
        filemenu.add_command(label='Figshare details', command=self.mFigshare)
        filemenu.add_command(label='Ssh details', command=self.mSsh)
        filemenu.add_command(label='Exit', command=self.mQuit)
        menubar.add_cascade(label='File',menu=filemenu)

        aboutmenu = Tkinter.Menu(menubar, tearoff = 0)
        aboutmenu.add_command(label='About', command=self.mAbout)
        aboutmenu.add_command(label='License', command=self.mLicense)
        menubar.add_cascade(label='About', menu=aboutmenu)
        self.config(menu=menubar)

    def onPrivacyCheck(self):
        if not self.privacyInt.get():
            tkMessageBox.showwarning('Public data warning',
                                     'Public data can not be deleted or made private, once published')
   

    def check_Files(self, directory):
 
        file_dict = {}
 
        for root, dirs, files in os.walk(directory):
            for file in files:
                name, ext =  os.path.splitext(file)
                if (ext):
                    if (ext in file_dict):
                        #file_dict[ext].append(directory + '/' + name +ext)
                        file_dict[ext].append(os.path.join(directory,name+ext))
                    else:
                        file_dict[ext] = [os.path.join(directory,name+ext)]
   
        return file_dict

    def publish_dict(self, pathLists):
        file_dict = {}
        for pathList in pathLists:
            files = pathList.split(';')
            for file in files:
                name, ext =  os.path.splitext(file)
                if (ext):
                    if (ext in file_dict):
                        file_dict[ext].append(name+ext)
                    else:
                        file_dict [ext] = [name+ext]
        return file_dict
        



    def runTelemac(self, file_dict):
        sel_path = None
        cli_path = None
        cas_path = None
        res_path = None
        coords = self.coordVariable.get()
	name = self.nameVariable.get()

        if file_dict.has_key('.cas'):
            cas_path = file_dict['.cas'][0]
            sel,cli,res = ParserTelemac.getFilePaths(cas_path)
            for  key in file_dict:
                for entry in file_dict[key]:
                    if sel in entry:
                        sel_path = entry
                    if cli in entry:
                        cli_path = entry
                    if res in entry:
                        res_path = entry            
        
        

        else:
            #This will lead to a bug where if the first file is not the executable
            print('could not find cas file doing without')
            if file_dict.has_key('.sel'):
                sel_path = file_dict['.sel'][0]

            if file_dict.has_key('.res'):
                sel_path = file_dict['.res'][0]

            if file_dict.has_key('.slf'):
                sel_path = file_dict['.slf'][0]

            if file_dict.has_key('.cas'):
                cas_path = file_dict['.cas'][0]
        
            if file_dict.has_key('.cli'):
                cli_path = file_dict['.cli'][0]


        sim = Telemac.Simulation(cli_path,sel_path, cas_path, res_path, coords, name)
        self.kml = sim.kml_path
        return


    def runFluidity(self, file_dict):
        sim = visualiser.simulation(file_dict)
        self.kml = sim.kml


    def ChooseSimulationType(self):
        Fluidity = False
        Telemac = False
        file_dict = self.publish_dict([self.configurationVariable.get(),self.inputVariable.get(),self.meshVariable.get(),self.outputVariable.get()])
        coords = self.coordVariable.get()

        for key in self.telemacExtensions:
            if file_dict.has_key(key):
                Telemac = True


        for key in self.fluidityExtensions:
            if file_dict.has_key(key):
                Fluidity = True

        if (Fluidity and Telemac):
            tkMessageBox.showerror('Error', 'Both Fluidity and Telemak files are present in your directory please be more specific')
        if not (Fluidity or Telemac):
            tkMessageBox.showerror('Error', 'No files for generating the KML are present')
        if Fluidity:
            self.runFluidity(file_dict)
        if Telemac:
            self.runTelemac(file_dict)

        return
        
        
           
    def parse_cas(self, cas_path, keywords = ['INITIAL CONDITIONS', 'INITIAL ELEVATION', 'CORIOLIS', 'OPTION FOR LIQUID BOUNDARIES']):
        Parser  = ParserTelemac.Parser(path = cas_path)
        results = Parser.search_keywords(fields = keywords)
        database = db.database( ([key for key in results] ))
        database.add_row([results[key] for key in results])
        print('after parsing cas', results)
        return

    def OnButtonClick(self):
        ## Import pyRDM

        self.ChooseSimulationType()
        publicationMethod = self.publicationMethod.get()
        print(publicationMethod)
        if publicationMethod == 'SSH':
            self.onSSH()
        elif publicationMethod == 'FigShare':
            self.onFigshare()

    def onSSH(self):
        self.password = pwdQuerry(self)
        return


    def onFigshare(self):
        try:
            import pyrdm
        except ImportError:
            tkMessageBox.showerror('Error', 'Unable to import pyRDM. Please ensure it is in your PYTHONPATH')
            return
        try:
            from pyrdm.publisher import Publisher
        except ImportError:
            tkMessageBox.showerror('Error', 'Please ensure you have installed all pyRDM dependencies. See manual for more details.')
            return
        print('publishing using figshare')
        self.publisher = Publisher(service="figshare")

        self.toBePublished = {'name': None, 'source': None, 'version': None, 'input': None, 'output': None, 'privacy': None}
        #name
        if self.nameVariable.get() == '':
            tkMessageBox.showerror('Error', 'Software name can not be empty.')
        else:
            self.toBePublished['name'] = self.nameVariable.get()
        #git source - check if we are uploading all software (or just data)
        print('still publishing 2')

        if (self.turn.get()):
            if self.gitDirectory.get() == '':
                print('inside the self.turn if statement')
                tkMessageBox.showerror('Error', 'Git source location can not be empty.')
                return
            else:
                self.toBePublished['source'] = self.gitDirectory.get()
        print('just before the version variable statement')

        '''
        print(self.versionVariable.get())
        if self.versionVariable.get() == '':
            tkMessageBox.showerror('Error', 'Version number can not be empty.')
            return
        else:
            self.toBePublished['version'] = self.versionVariable.get()'''

        if self.versionVariable.get():
            self.toBePublished['version'] = self.versionVariable.get()


        #input
        self.toBePublished['input'] = self.inputVariable.get().split(';')
        if self.kml:
            self.toBePublished['input'].append(self.kml)
        #output
        self.toBePublished['output'] = self.outputVariable.get()
        #privacy
        self.toBePublished['privacy'] = bool(self.privacyInt.get())

        ## Checks if we should publish software or just data (using the check button on the GUI)

        ## Assign None to pid in case we only publish data and do not have a pid already
        pid = None
        print('still publishing 1')
        if self.turn.get():
            print 'Publishing software through git.'
            pid, doi = self.publisher.publish_software(name=self.toBePublished['name'],
                                                       version=self.toBePublished['version'],
                                                       private=self.toBePublished['privacy'])

        ## Publishes the data/files specified in the files (input) section

        print('input', self.toBePublished['input'])

        if self.toBePublished['input'] is not '':
            parameters={}
            parameters['title'] = self.toBePublished['name']
            parameters['description'] = 'Input Data'
            parameters['files'] = self.toBePublished['input']

            ## Check if categories and tags have been assigned from the publish software function, if not (and it's data alone) add default tags

            if not self.turn.get():
                parameters['tag_name'] = ['Data']
                parameters['category'] = 'Uncategorised'

            #pid, doi = self.publisher.publish_data(parameters=parameters,
            #                                      pid = pid,
            #                                       private=self.toBePublished['privacy'])
            print('publishingdata')
            pid, doi = self.publisher.publish_data(parameters=parameters,
                                                   pid=pid,
                                                   private=self.toBePublished['privacy'])

        print 'pid', pid
        print 'doi', doi

        self.labelVariable.set( 'Status: Published!')
        return

    def OnFetchGit(self):
        try:
            from pyrdm.git_handler import GitHandler
        except ImportError:
            print 'Import Error: Ensure python path includes pyrdm'
            return

        try:
            repository = git.Repo(self.gitDirectory.get())
        except git.InvalidGitRepositoryError:
            tkMessageBox.showwarning('Git path warning', 'Invalid git repository - please check path!')
            return

        git_handler = GitHandler(self.gitDirectory.get())
        self.versionVariable = git_handler.get_head_version()
        return

    def OnPressEnter(self,event):
        print 'Nothing happens when you press Enter'
        #self.labelVariable.set( self.entryVariable.get())

    def mFigshare(self):
        figshare = figshareDetails(self)
        figshare.focus_set()
    def mSsh(self):
        Ssh = SshDetails(self)
        Ssh.title('Ssh Authentication details')
        Ssh.focus_set()
        

    def mOpen(self):
        self.entryVariable.set(tkFileDialog.askopenfilename())

    def mGitOpen(self):
        self.gitDirectory.set(tkFileDialog.askdirectory())
        self.OnFetchGit()

    def mOpenDirectory(self):
        self.sourceVariable.set(tkFileDialog.askdirectory())

    def mOpenConfiguration(self):
        entry = tkFileDialog.askopenfilename()
        temp = self.configurationVariable.get()
        if entry and temp:
            self.configurationVariable.set(temp + ';' + entry)
        elif entry:
            self.configurationVariable.set(entry)

    def mOpenMesh(self):
        entry = tkFileDialog.askopenfilename()
        temp = self.meshVariable.get()
        if entry and temp:
            self.meshVariable.set(temp + ';' + entry)
        elif entry:
            self.meshVariable.set(entry)

    def mOpenInput(self):
        entry = tkFileDialog.askopenfilename()
        temp = self.inputVariable.get()
        if entry and temp:
            self.inputVariable.set(temp + ';' + entry)
        elif entry:
            self.inputVariable.set(entry)

    def mOpenOutput(self):
        entry = tkFileDialog.askopenfilename()
        temp = self.outputVariable.get()
        if entry and temp:
            self.outputVariable.set(temp + ';' + entry)
        elif entry:
            self.outputVariable.set(entry)


    def mAutopopulate(self):
        self.onClear()
        directory = self.sourceVariable.get()
        file_dict = self.check_Files(directory)
        #We should be careful that the program doesn't add files from both filetypes (example .mesh and .cli)
        if file_dict:
            #input is a python keyword so use vInput
            #Need to be able to append multiplefiles seperated by a ; so cannot set variable directly
            print('file_dict', file_dict)
            for extension in self.extensions['meshExtensions']:
                print('extension',extension)
                if extension in file_dict:
                    temp = self.meshVariable.get()
                    if temp:
                        self.meshVariable.set(temp+';'+';'.join(file_dict[extension]) )
                    else:
                        self.meshVariable.set(';'.join(file_dict[extension]) )
            for extension in self.extensions['configurationExtensions']:
                if extension in file_dict:
                    temp = self.configurationVariable.get()
                    if temp:
                        self.configurationVariable.set(temp+';'+';'.join(file_dict[extension]) )
                    else:
                        self.configurationVariable.set(';'.join(file_dict[extension]) )
            for extension in self.extensions['inputExtensions']:
                if extension in file_dict:
                    temp = self.inputVariable.get()
                    if temp:
                        self.inputVariable.set(temp+';'+';'.join(file_dict[extension]) )
                    else:
                        self.inputVariable.set(';'.join(file_dict[extension]) )
            for extension in self.extensions['outputExtensions']:
                if extension in file_dict:
                    temp = self.outputVariable.get()
                    if temp:
                        self.outputVariable.set(temp+';'+';'.join(file_dict[extension]) )
                    else:
                        self.outputVariable.set(';'.join(file_dict[extension]) )

            #Flatten the respective lists 
            '''self.Mesh=[item for sublist in self.Mesh for item in sublist]
            self.Configuration=[item for sublist in self.Configuration for item in sublist]
            self.Input=[item for sublist in self.Input for item in sublist]
            self.Output=[item for sublist in self.Output for item in sublist]

            self.meshVariable.set(';'.join(self.Mesh))
            self.configurationVariable.set(';'.join(self.Configuration))
            self.inputVariable.set(';'.join(self.Input))
            self.outputVariable.set(';'.join(self.Output))'''

        return 'to be continued'
        
    

    def mQuit(self):
        if tkMessageBox.askyesno('Quit', 'Are you sure?'):
            self.destroy()

    def mAbout(self):
        pass

    def mLicense(self):
        licenseWindow = LicenseDetails(self)
        licenseWindow.title('License  - GNU General Public License v3')


class SshDetails(Tkinter.Toplevel):
    def __init__(self, parent):
        Tkinter.Toplevel.__init__(self, parent)
        self.parent = parent

        self.grid()
        ## 3 fields: name, affiliation
        ##           token

        # Token ID
        self.label = Tkinter.Label(self, text="Remote Machine:")
        self.label.grid(column=0, row=0, sticky='w')

        self.remoteVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.remoteVariable, width=50)
        self.entry.grid(column=1, row=0, sticky='w')

        # Communication Port
        self.label = Tkinter.Label(self, text="port:")
        self.label.grid(column=0, row=1, sticky='w')

        self.portVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.portVariable, width=50)
        self.entry.grid(column=1, row=1, sticky='w')

        #Username
        self.label = Tkinter.Label(self, text='username:')
        self.label.grid(column=0, row=2, sticky='w')

        self.userVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.userVariable, width=50)
        self.entry.grid(column=1, row=2, sticky='w')

        # Directory on remote machine
        self.label = Tkinter.Label(self, text="Directory:")
        self.label.grid(column=0, row=3, sticky='w')

        self.directoryVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.directoryVariable, width=50)
        self.entry.grid(column=1, row=3, sticky='w')

        # save options button
        self.button = Tkinter.Button(self, text=u'Save Authentication Details', command=self.onSshSaveClick)
        self.button.grid(column=1, row=4, sticky='E')

        self.loadSshConfig()

        self.resizable(False, False)


    def onSshSaveClick(self):
        self.config = ConfigParser.RawConfigParser()
        self.configfile = self.config.read(os.path.join(os.path.expanduser('~'), ".config", "cml.ini"))

        self.config.set("general", "remote host", self.remoteVariable.get())
        self.config.set("general", "port", self.portVariable.get())
        self.config.set("general", "username", self.userVariable.get())
        self.config.set("general", "directory", self.directoryVariable.get())


        try:
            fileobject = open(os.path.join(os.path.expanduser('~'), ".config", "cml.ini"), 'w')
        except IOError:
            print 'Cannot open file!'
        else:
            self.config.write(fileobject)
            fileobject.close()

        print "Saved authentication details!"

    def loadSshConfig(self):
        self.config = ConfigParser.ConfigParser()
        self.have_config = (self.config.read(os.path.join(os.path.expanduser('~'), ".config", "cml.ini")) != [])
        if self.have_config:
            self.remoteVariable.set(self.config.get("general", "remote host"))
            self.portVariable.set(self.config.get("general", "port"))
            self.userVariable.set(self.config.get("general", "username"))
            self.directoryVariable.set(self.config.get("general", "directory"))
        else:
            print 'Unable to find cml.ini file - creating the file!'

            self.config.add_section('general')
            self.config.set('general', 'username', '')
            self.config.set('general', 'remote host', '')
            self.config.set('general', 'directory', '')
            self.config.set('general', 'port', '')

            try:
                fileobject = open(os.path.join(os.path.expanduser('~'), ".config", "cml.ini"), 'w')
            except IOError:
                print 'Cannot open file!'
            else:
                self.config.write(fileobject)
                fileobject.close()





            ## ADD ERROR CHECKING HERE

class figshareDetails(Tkinter.Toplevel):
    def __init__(self,parent):
        Tkinter.Toplevel.__init__(self,parent)
        self.parent = parent

        self.grid()
        ## 3 fields: name, affiliation
        ##           token

        #Token ID
        self.label = Tkinter.Label(self, text="Token:")
        self.label.grid(column=0,row=0, sticky='w')
        
        self.tokenVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.tokenVariable, width = 50)
        self.entry.grid(column=1,row=0, sticky='w')

        #Authors
        self.label = Tkinter.Label(self, text="Authors:")
        self.label.grid(column=0,row=1, sticky='w')
        
        self.authorVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.authorVariable, width = 50)
        self.entry.grid(column=1,row=1, sticky='w')

        #Affiliation
        self.label = Tkinter.Label(self, text="Affiliations:")
        self.label.grid(column=0,row=2, sticky='w')
        
        self.affiliationVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=self.affiliationVariable, width = 50)
        self.entry.grid(column=1,row=2, sticky='w')

        #save options button
        self.button = Tkinter.Button(self,text=u'Save Authentication Details', command=self.onFigshareSaveClick)
        self.button.grid(column=1,row=4,sticky='E')
        
        self.loadFigshareConfig()
        
        self.resizable(False,False)


    def onFigshareSaveClick(self):
        self.config = ConfigParser.RawConfigParser()
        self.configfile = self.config.read(os.path.join(os.path.expanduser('~'), ".config", "pyrdm.ini"))

        self.config.set("figshare", "token", self.tokenVariable.get())
        self.config.set("general", "name", self.authorVariable.get())
        self.config.set("general", "affiliation", self.affiliationVariable.get())

        try:
            fileobject = open(os.path.join(os.path.expanduser('~'), ".config", "pyrdm.ini"),'w')
        except IOError:
            print 'Cannot open file!'
        else:
            self.config.write(fileobject)
            fileobject.close()
        
    
        print "Saved authentication details!"
    
        ## ADD ERROR CHECKING HERE:q



    def loadFigshareConfig(self):
        self.config = ConfigParser.ConfigParser()
        self.have_config = (self.config.read(os.path.join(os.path.expanduser('~'), ".config", "pyrdm.ini")) != [])
        if self.have_config:
            self.tokenVariable.set(self.config.get("figshare", "token"))
            self.authorVariable.set(self.config.get("general", "name"))
            self.affiliationVariable.set(self.config.get("general", "affiliation"))
        else:
            print 'Unable to find pyrdm.ini file - creating the file!'
            
            self.config.add_section('general')
            self.config.set('general', 'name', '')
            self.config.set('general', 'affiliation', '')

            self.config.add_section('figshare')
            self.config.set('figshare', 'token', '')

            self.config.add_section('zenodo')
            self.config.set('zenodo', 'access_token', 'abc')

            self.config.add_section('dspace')
            self.config.set('dspace', 'user_name', 'abc')
            self.config.set('dspace', 'user_pass', 'xyz')
            self.config.set('dspace', 'service_document_url', 'http://example.org/swordv2/servicedocument')
            self.config.set('dspace', 'collection_title', 'My Collection Title Here')

            try:
                fileobject = open(os.path.join(os.path.expanduser('~'), ".config", "pyrdm.ini"),'w')
            except IOError:
                print 'Cannot open file!'
            else:
                self.config.write(fileobject)
                fileobject.close()

class LicenseDetails(Tkinter.Toplevel):
    def __init__(self,parent):
        Tkinter.Toplevel.__init__(self,parent)
        self.parent = parent

        self.grid()
        licenseStr = '''
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
        '''
        self.label = Tkinter.Label(self, text=licenseStr)
        self.label.grid(column=0,row=0, sticky='w')

class pwdQuerry(Tkinter.Toplevel):
    def __init__(self, parent):
        Tkinter.Toplevel.__init__(self, parent)
        self.parent = parent
        self.grid()

        self.label = Tkinter.Label(self, text='Enter Password')
        self.label.grid(column=0, row=0, sticky='w')

        pwd = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self, textvariable=pwd, width=50)
        self.entry.config(show="*")
        self.entry.grid(column=0, row=1, sticky='w')


        self.button = Tkinter.Button(self, text=u'Submit', command=lambda :self.submitSsh(pwd.get() , self.parent.nameVariable.get()))
        self.bind('<Return>', lambda x: self.submitSsh(pwd.get(), self.parent.nameVariable.get() ) )
        self.button.grid(column=1, row=4, sticky='E')




        return

    def submitSsh(self, pwd, name):
        try:
            import paramiko
        except ImportError:
            tkMessageBox.showerror('Error', 'Unable to import paramiko')

        files = self.getFileList()

        self.config = ConfigParser.RawConfigParser()
        self.configfile = self.config.read(os.path.join(os.path.expanduser('~'), ".config", "cml.ini"))
        username = self.config.get('general', 'username')
        host = self.config.get('general', 'remote host')
        remotepath = self.config.get('general', 'directory')
        port = self.config.get('general', 'port')


        try:
            transport = paramiko.Transport((host, int(port)))
            try:
                transport.connect(username=username, password=pwd)
                sftp = paramiko.SFTPClient.from_transport(transport)
                # We put all the simulations in a directory found at remotePath/nameVariable/
                try:
                    sftp.chdir(os.path.join(remotepath, name))  # Test if remote_path exists
                except IOError:
                    sftp.mkdir(os.path.join(remotepath, name))  # Create remote_path
                    sftp.chdir(os.path.join(remotepath, name))

                try:
                    for localpath in files:
                        sftp.put(localpath, os.path.join(remotepath, name, os.path.basename(localpath)))
                    print('checkbox is')
                    if self.parent.turn.get():
                        softwareDirectory = self.parent.gitDirectory.get()
                        shutil.make_archive(name, 'zip', softwareDirectory)
                        rootfolder = os.path.dirname(os.path.realpath(__file__))
                        shutil.move(os.path.join(rootfolder, name+'.zip'), os.path.join(rootfolder, 'Simulations', 'zipFiles') )
                        sftp.put(os.path.join(rootfolder, 'Simulations', 'zipFiles',name+'.zip'), os.path.join(remotepath, name, name+'.zip') )
                        print(os.path.join(remotepath, name, name+'.zip'))
                        #sftp.put()





                    tkMessageBox.showwarning('Success',
                                             'Secure File Transfer was succesful')
                    self.destroy()
                except Exception as e:
                    tkMessageBox.showwarning('Error',
                                             'Something went wrong in storing the files, perhaps the path given does not exist')
                    print(e)



            except:
                tkMessageBox.showwarning('Error',
                                         'Could not connect using the given password and user combination')

        except:
            tkMessageBox.showwarning('Error',
                                     'Could not find host,port combination, you can set this in ssh details in the top left menu')



    def zipFolder(self, directory , name):
        shutil.make_archive(name, 'zip', directory, name)
        shutil.move(src, dst)

    def getFileList(self):
        files = []
        files+=self.parent.inputVariable.get().split(';')
        files+=self.parent.configurationVariable.get().split(';')
        files+=self.parent.meshVariable.get().split(';')
        files+=self.parent.outputVariable.get().split(';')
        files.append(self.parent.kml)
        print('THE FILE LIST IS')
        print(files)
        return files


if __name__=="__main__":
    app = pyRDM_GUI(None)
    app.title('pyRDM GUI')

    try:
        logo_path =os.path.realpath(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),'Images','logo.ico')))
        app.wm_iconbitmap(logo_path)

    except Tkinter.TclError:
        print 'No icon found.'
    app.mainloop()


def findDirectoryStructure(self, rootdir):
    #returns dictionary of form {'path':[file1,file2]}
    directories={}
    for root, subFolders, files in os.walk(rootdir):
        directories[os.path.relpath(root, rootdir)]=files
    return directories



def SshDirectories(sftp, directories, remotepath, localpath):
    for dir in directories:
        SshMakeDir( os.path.join(remotepath,dir) )
        for file in directories[dir]:
            sftp.put(localpath, os.path.join(remotepath, name, os.path.basename(localpath)))


def SshMakeDir(sftp, directoryPath):
    try:
        sftp.chdir(directoryPath)  # Test if remote_path exists
    except IOError:
        sftp.mkdir(directoryPath)  # Create remote_path
        sftp.chdir(directoryPath)
