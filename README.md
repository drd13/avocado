# README #

##Description##

The project is a program designed to make the sharing and visualization of numerical simulations easier. By using the graphical interface it is possible to convert numerical simulations written for Telemac2D or Fluidity into files that can be visualized through Google-Earth. In addition the files as well as the computer code used to generate them can be automatically loaded into a database through Secure File Transfer or loaded online with a pid or doi through the Figshare website.

##How to use the program##

If the program is to be used in conjunction with an external database the first step is to edit the SSh or Figshare details. To do this click on the top-left 'filemenu' and edit the details.

To upload a simulation, select the directory containing the simulation and click on the auto-populate button. This will populate the different fields (Mesh, Input...). It is then possible to modify the files to be submitted or add some files that are missing. To also publish the software tick the 'add software' check-box and select the directory containing the software. If the software is to be published through pyrdm, the selected directory must be a git directory (talk about public/private checkbox or modify GUI).

When all of this is done click on the Publish Button, this will create a kmz file readable by Google-Earth within the 'KML' sub directory and upload all the data in the specified folder in a sub-folder given by the 'Name' text entry.

In order to test the program, two simulations, one from fluidity and one from telemac are available in the simulation subfolder. Both simulations should run smoothly if all the sub dependencies and details are entered correctly  


##Dependencies##

This program requires:

* mysql
* pyrdm
* numpy
* scipy
* matplotlib
* parserSELAFIN (from Telemac)
* osgeo (ogr,osr, gdal)
* simplekml
* gitPython


##Contributors##
The people who have worked on this project are:

* Simon Mouradian
* Matthew Piggott
* Damien de Mijolla
* Jan Lietava
* Alexandros Avdis

##License##

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
